from django.contrib import admin
from . import models


@admin.register(models.User)
class UserAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'first_name',
        'last_name',
        'email',
    )
    list_filter = ('email', 'first_name', 'last_name')
    search_fields = ('email', 'first_name', 'last_name')


@admin.register(models.Post)
class PostAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'title',
        'user',
        'is_private',
    )
    list_filter = ('title', )
    search_fields = ('title',)


@admin.register(models.Like)
class LikeAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'post',
        'user',
    )
    list_filter = ('user__email', 'post__title')
    search_fields = ('user__email', 'post__title')