from django.contrib.auth.decorators import login_required
from django.utils import decorators as dj_decorators
from rest_framework import generics
from rest_framework.exceptions import PermissionDenied
from rest_framework_simplejwt.views import TokenViewBase
from rest_framework.viewsets import ModelViewSet
from . import serializers, models
from rest_framework import views
from rest_framework.response import Response
from django.db.models import Q
from rest_framework import status


class RegisterView(generics.ListCreateAPIView):
    permission_classes = ()
    queryset = models.User.objects.all()
    serializer_class = serializers.UserRegisterSerializer

    def get_serializer_class(self):
        if self.request.method.lower() == 'get':
            return serializers.UserSerializer
        if self.request.method.lower() == 'post':
            return serializers.UserRegisterSerializer
        return serializers.UserSerializer


class LoginView(TokenViewBase):
    serializer_class = serializers.LoginSerializer


# @dj_decorators.method_decorator(login_required, name="dispatch")
class PostAPIView(ModelViewSet):
    serializer_class = serializers.PostSerializer
    queryset = models.Post.objects.all()
    page_size = 10

    def get_serializer_class(self):
        if self.action == 'list':
            return serializers.ReadPostSerializer
        if self.action == 'retrieve':
            return serializers.ReadPostSerializer
        return serializers.PostSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(Q(is_private=False) | Q(user=request.user, is_private=True))
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', True)
        instance = self.get_object()

        if instance.user != request.user:
            raise PermissionDenied("You do not have permission to update this post.")

        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.user != request.user:
            raise PermissionDenied("You do not have permission to delete this post.")
        self.perform_destroy(instance)
        return Response(data="successfully deleted", status=status.HTTP_204_NO_CONTENT)


class LikeAPIView(views.APIView):

    def post(self, request, *args, **kwargs):
        user = request.user
        post = generics.get_object_or_404(models.Post.objects.filter(Q(is_private=False) | Q(user=user, is_private=True)), id=kwargs.get("id"))
        if models.Like.objects.filter(user_id=user.id, post_id=post.id).exists():
            models.Like.objects.filter(user_id=user.id, post_id=post.id).delete()
            return Response(data="You unliked this post")

        else:
            models.Like.objects.create(user_id=user.id, post_id=post.id)
        return Response(data="You liked this post")
