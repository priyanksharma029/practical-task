from rest_framework import serializers
from . import models
from django.contrib.auth.password_validation import validate_password
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer


class UserSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = models.User
        fields = ['name', 'email', 'date_joined', 'is_active']

    def get_name(self, obj):
        return obj.get_full_name


class UserRegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password],
                                     style={'input_type': 'password'})
    password1 = serializers.CharField(write_only=True, required=True, validators=[validate_password],
                                      style={'input_type': 'password'})

    class Meta:
        model = models.User
        fields = ['first_name', 'last_name', 'email', 'password', 'password1']

    def validate(self, attrs):
        if attrs['password'] != attrs['password1']:
            raise serializers.ValidationError({'password': "password field didn't match"})
        return attrs

    def create(self, validated_data):
        user = models.User.objects.create(email=validated_data['email'], first_name=validated_data['first_name'],
                                          last_name=validated_data['last_name'])

        user.set_password(validated_data['password'])
        user.is_active = True
        user.save()
        return user


class LoginSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)
        refresh = self.get_token(self.user)
        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)
        data['user'] = UserSerializer(self.user).data
        return {'access': data['access'], 'refresh': data['refresh'], 'user': data['user']}


class PostSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField()

    class Meta:
        model = models.Post
        fields = '__all__'

    def get_likes(self, obj):
        post_like_instance = models.Like.objects.filter(post_id=obj.id)
        if not post_like_instance.exists():
            return 0
        return post_like_instance.count()


class ReadPostSerializer(serializers.ModelSerializer):
    likes = serializers.SerializerMethodField()
    user = serializers.StringRelatedField()

    class Meta:
        model = models.Post
        fields = '__all__'

    def get_likes(self, obj):
        post_like_instance = models.Like.objects.filter(post_id=obj.id)
        if not post_like_instance.exists():
            return 0
        return post_like_instance.count()