from django.urls import path
from rest_framework import routers
from . import views

app_name = "blog"

router = routers.DefaultRouter()
router.register('post', views.PostAPIView, basename="post")

urlpatterns = [
    path('register/', views.RegisterView.as_view(), name="register"),
    path('login/', views.LoginView.as_view(), name="login"),
    path("like-dislike/<int:id>/", views.LikeAPIView.as_view(), name="like_dislike"),
]

urlpatterns += router.urls
